/**
  Generated Pin Manager File

  Company:
    Microchip Technology Inc.

  File Name:
    pin_manager.c

  Summary:
    This is the Pin Manager file generated using MPLAB® Code Configurator

  Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB® Code Configurator - v2.25.2
        Device            :  PIC16LF1823
        Driver Version    :  1.02
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.34
        MPLAB             :  MPLAB X v2.35 or v3.00
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#include <xc.h>
#include "pin_manager.h"
#include "tmr1.h"
#include "memory.h"

void PIN_MANAGER_Initialize(void) {
    LATA = 0x00;
    TRISA = 0x0F;
    ANSELA = 0x00;
    WPUA = 0x00;

    LATC = 0x00;
    TRISC = 0x00;
    ANSELC = 0x00;
    WPUC = 0x00;

    OPTION_REGbits.nWPUEN = 0x01;

    APFCON = 0x00;

    // enable interrupt-on-change individually    
    IOCAN2 = 1;

    // enable interrupt-on-change globally
    INTCONbits.IOCIE = 1;

}

char bit_number = 0;

unsigned int prev_timer_value = 0;
const unsigned start_pulse[] = { 
    6500, 7000
};

const unsigned zero_length[] = { 
    530, 590
};

const unsigned one_length[] = { 
    1000, 1200
};

unsigned char data[4];

void PIN_MANAGER_IOC(void) {
    unsigned int time, delta;
    unsigned char index;

    if (IOCAF2 == 1) {
        //@TODO Add handling code for IOC on pin RA2
        time = TMR1_ReadTimer();
        
        delta = time - prev_timer_value;
        prev_timer_value = time;
        if(bit_number == 0 && delta > start_pulse[0] && delta < start_pulse[1])
        {
            bit_number++;
            data[0] = data[1] = data[2] = data[3] = 0;
        }
        else if(bit_number <= 32)
        {
            index = (bit_number-1) >> 3;
            if(delta > one_length[0] && delta < one_length[1])
            {
                data[index] |= 1<<((bit_number-1) & 7);
                bit_number++;
            }
            else if(delta > zero_length[0] && delta < zero_length[1])
            {
                bit_number++;
            }
            else 
            {
                bit_number = 0;
            }
        }    
        else
        {
            bit_number = 0;
        }
      
        
        if(bit_number == 32)
        {
            if(data[1] == 0xAF && data[2] == 0x1d && data[3] == 0x62)
            {
                buzzer_enable = 1;
                prev_timer_value = 0;
                TMR1_WriteTimer(0);
                RC0 = !RC0;           
            }

            bit_number = 0;
        }

        // clear interrupt-on-change flag
        IOCAF2 = 0;
    }
}
/**
 End of File
 */