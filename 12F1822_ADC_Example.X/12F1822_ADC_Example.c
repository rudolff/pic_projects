//  Software License Agreement
//
// The software supplied herewith by Microchip Technology Incorporated (the "Company")
// for its PICmicro� Microcontroller is intended and supplied to you, the Company?s
// customer, for use solely and exclusively on Microchip PICmicro Microcontroller
// products.
//
// The software is owned by the Company and/or its supplier, and is protected under
// applicable copyright laws. All rights are reserved. Any use in violation of the
// foregoing restrictions may subject the user to criminal sanctions under applicable
// laws, as well as to civil liability for the breach of the terms and conditions of
// this license.
//
// THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES, WHETHER EXPRESS,
// IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE
// COMPANY SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
// CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
////


//**********************************************************************************
// Example program showing how to set up the ACD on a PIC12F1822
//
// Device: PIC12F1822
// Demo Board: PICkit 2 Low Pin Count Demo Board + AC244043 header board for debugging
// Compiler: Microchip XC8 v1.11
// IDE: MPLAB X v1.5
// Created: January 2013
//
// This program shows how to set up the ADC to capture an analog voltage on the
// RA0 pin of the PIC12F1822.  We used the PICkit2 Low Pin Count Demo board
// for this example which has a potentiometer already connected to the RA0 pin.
// The result of the ADC is a 10 bit number which will then
// be copied into the duty cycle register of the PWM.  The PWM output (RA2)
// was connected to an external LED (thru 200 ohm resistor) so as you adjust
// the position of the potentiometer, the brightness of the LED will change.
//
// NOTE: If you want to do debugging with the PIC12F1822 using a debugger,
// you will need to obtain the AC244043 (or AC244044 for 'LF' device)
// "header board" which will allow you to use all the pins on the device while
// you are debugging.  
//
//**********************************************************************************
//          PIC12F1822 Pinout for this example
//          ----------
//      Vdd |1      8| GND
//      RA5 |2      7| RA0 <- analog input from potentiomter (0-5V)
//      RA4 |3      6| RA1
//      RA3 |4      5| RA2 -> PWM output connected to LED thru 200 Ohm resistor
//          ----------
//
//***********************************************************************************

#include <xc.h> // include standard header file



// Definitions
#define _XTAL_FREQ  16000000        // this is used by the __delay_ms(xx) and __delay_us(xx) functions

//**********************************************************************************
// This subroutine takes in a 10 bit number and sets the duty cycle register
// for the PWM accordingly
//**********************************************************************************
void SetPWMDutyCyle(unsigned int duty_cycle_value)
{
    CCP1CONbits.DC1B = duty_cycle_value & 0x03; //first set the 2 lsb bits
    CCPR1L =  (duty_cycle_value >> 2);           //now set upper 8 msb bits
}

//**********************************************************************************
// This subroutine does the ADC conversion and returns the 10 bit result
//**********************************************************************************
unsigned int Read_ADC_Value(void)
{
    unsigned int ADCValue;
    
    ADCON0bits.GO = 1;              // start conversion
    while (ADCON0bits.GO);          // wait for conversion to finish
    ADCValue = ADRESH << 8;         // get the 2 msbs of the result and rotate 8 bits to the left
    ADCValue = ADCValue + ADRESL;   // now add the low 8 bits of the resut into our return variable
    return (ADCValue);              // return the 10bit result in a single variable
}

//**********************************************************************************
//*****************   main routine   ***********************************************
//**********************************************************************************
void main ( ) 
{

    unsigned int AnalogValue;       // used to store ADC result after capture

    // set up oscillator control register
    OSCCONbits.SPLLEN=0;    // PLL is disabled
    OSCCONbits.IRCF=0x0F;   //set OSCCON IRCF bits to select OSC frequency=16Mhz
    OSCCONbits.SCS=0x02;    //set the SCS bits to select internal oscillator block
    // OSCON should be 0x7Ah now.

    // Set up I/O pins

    // PORT A Assignments
    TRISAbits.TRISA0 = 1;	// RA0 = Analog Voltage In
    TRISAbits.TRISA1 = 0;	// RA1 = nc
    TRISAbits.TRISA2 = 0;	// RA2 = PWM Output (CCP1) connected to LED
    TRISAbits.TRISA3 = 0;	// RA3 = nc (MCLR)
    TRISAbits.TRISA4 = 0;	// RA4 = nc
    TRISAbits.TRISA5 = 0;	// RA5 = nc

    DACCON0bits.DACEN=0;        // turn DAC off
    
    // Set up ADC
    ANSELAbits.ANSA0=1;		// Select A0 as analog input pin for potentiometer input
                                // You will need to set the ANSA bits for each pin you want
                                // to use as analog inputs

    ADCON0bits.CHS=0x00;	// This selects which analog input to use for the ADC conversion
                                // for this example we are using A0 as our input
    ADCON0bits.ADON=1;		// ADC is on
    ADCON1bits.ADCS=0x01;	// select ADC conversion clock select as Fosc/8
    ADCON1bits.ADFM=0x01;       // results are right justified
    

    // Now setup PWM
    //******************************************************************************************
    // PWM Period = (1/Fosc) * 4 * (TMR2 Prescaler)* (PR2+1)
    //******************************************************************************************
    // Here are sample PWM periods for different TMR2 Prescalar values for Fosc=16Mhz and PR2=255
    //******************************************************************************************
    // TMR2 Prescalar=1: PWM Period = (1/16000000)*4*1*256 = 64 us or 15.63 khz
    // TMR2 Prescalar=4: PWM Period = (1/16000000)*4*4*256 = 256 us or 3.91 khz
    // TMR2 Prescalar=16: PWM Period = (1/16000000)*4*16*256= 1.024 ms or .976 khz
    // TMR2 Prescalar=64: PWM Period = (1/16000000)*4*64*256= 4.096 ms or .244 khz
    //
    // For this example we will choose the PWM period of 64us (15.63 kHz) so most people
    // will not be able to hear it.

    // ***** Setup PWM output ******************************************************

    TRISAbits.TRISA2 = 1;       // disable pwm pin output for the moment

    CCP1CONbits.CCP1M=0x0C;     // select PWM mode for CCP module
    CCP1CONbits.P1M=0x00;	// select single output on CCP1 pin (RA5)

    PR2 = 0xff;                 // set PWM period as 255 per our example above

    CCPR1L =  0x00;             // clear high 8 bits of PWM duty cycle
    CCP1CONbits.DC1B=0x00;	// clear low 2 bits of PWM Duty cycle

                                // Note: PWM uses TMR2 so we need to configure it
    PIR1bits.TMR2IF=0;		// clear TMR2 interrupt flag
    T2CONbits.T2CKPS=0x00;      // select TMR2 prescalar as divide by 1 as per our example above
    T2CONbits.TMR2ON=1;		// turn TMR2 on

    TRISAbits.TRISA2 = 0;	// turn PWM output back on


    //***********************************************************************************************************
    // This is our main loop.  We will read the 10 bit value from the ADC and put
    // that value into the duty cycle register of the PWM to change the brightness of the LED
    // so as you turn the potentiometer the brightness of the LED will change.
    //***********************************************************************************************************

    do{
        AnalogValue = Read_ADC_Value();
        SetPWMDutyCyle(AnalogValue);
        __delay_ms(100);
      
    } while (1);


}




