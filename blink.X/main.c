/*
 * File:   main.c
 * Author: Виталий
 *
 * Created on 7 февраля 2016 г., 22:27
 */

#define _XTAL_FREQ 4000000

#include <xc.h>

// BEGIN CONFIG
#pragma config "FOSC = INTOSCIO" // Oscillator Selection bits (HS oscillator)
#pragma config "WDTE = OFF" // Watchdog Timer Enable bit (WDT enabled)
#pragma config "PWRTE = ON" // Power-up Timer Enable bit (PWRT disabled)
#pragma config "MCLRE = OFF"      // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is digital input, MCLR internally tied to VDD)
#pragma config "BOREN = OFF" // Brown-out Reset Enable bit (BOR enabled)
#pragma config "LVP = OFF" // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config "CPD = OFF" // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config "CP = OFF" // Flash Program Memory Code Protection bit (Code protection off)
//END CONFIG


#define REORDERA(a)      (((a & 3)<<6) | ((a & 0x7C)>>2))
#define REORDER_BACKA(a) (((a & 0x7C)>>2) | ((a & 3)<<6))


// A, B, timeout 8 ms
const unsigned char STEPS_INIT[] = {
    0xFF, 0xFF, 10,
    0, 0, 10
};

const unsigned char STEPS_DROP[] = {
    // drop
    // PORTA, PORTB, Delay
    0b00000000, 0b00000001, 4,
    0b00010000, 0b00000010, 4,
    0b00001000, 0b00000100, 4,
    0b00000100, 0b00001000, 4,
    0b00000010, 0b00010000, 4,
    0b00000001, 0b00100000, 4,
    0b10000000, 0b01000000, 4,
    0b01000000, 0b00000000, 4,

    0b01000000, 0b00000001, 4,
    0b01010000, 0b00000010, 4,
    0b01001000, 0b00000100, 4,
    0b01000100, 0b00001000, 4,
    0b01000010, 0b00010000, 4,
    0b01000001, 0b00100000, 4,
    0b11000000, 0b01000000, 4,

    0b11000000, 0b01000001, 4,
    0b11010000, 0b01000010, 4,
    0b11001000, 0b01000100, 4,
    0b11000100, 0b01001000, 4,
    0b11000010, 0b01010000, 4,
    0b11000001, 0b01100000, 4,

    0b11000001, 0b01100001, 4,
    0b11010001, 0b01100010, 4,
    0b11001001, 0b01100100, 4,
    0b11000101, 0b01101000, 4,
    0b11000011, 0b01110000, 4,

    0b11000011, 0b01110001, 4,
    0b11010011, 0b01110010, 4,
    0b11001011, 0b01110100, 4,
    0b11000111, 0b01111000, 4,

    0b11000111, 0b01111001, 4,
    0b11010111, 0b01111010, 4,
    0b11001111, 0b01111100, 4,

    0b11001111, 0b01111101, 4,
    0b11011111, 0b01111110, 4,
    0b11011111, 0b01111111, 4
};

const unsigned char STEPS_VSHIFT[] = {
    0b01000000, 0b00000000, 2,
    0b11000000, 0b01000000, 2,
    0b11000001, 0b01100000, 2,
    0b11000011, 0b01110000, 2,
    0b11000111, 0b01111000, 2,
    0b11001111, 0b01111100, 2,
    0b11011111, 0b01111110, 2,
    0b11011111, 0b01111111, 2,
    0b10011111, 0b01111111, 2,
    0b00011111, 0b00111111, 2,
    0b00011110, 0b00011111, 2,
    0b00011100, 0b00001111, 2,
    0b00011000, 0b00000111, 2,
    0b00010000, 0b00000011, 2,
    0b00000000, 0b00000001, 2
};

unsigned int random() {
    static unsigned int seed = 32341;
    seed = (seed * 58321) + 11113;
    return (seed >> 16) % 0xFFFF;
}

void wait_hms(unsigned char count) {
    while (count--) {
        __delay_ms(100);
    }
}

void play(unsigned char * fx, unsigned char bytes_count, unsigned turns, unsigned char reversed) {
    while (turns--) {
        if (!reversed) {
            for (int i = 0; i < bytes_count; i += 3) {
                PORTA = fx[i];
                PORTB = fx[i + 1];

                wait_hms(fx[i + 2]);
            }
        } else {
            for (int i = bytes_count - 3; i >= 0; i -= 3) {
                PORTA = fx[i];
                PORTB = fx[i + 1];

                wait_hms(fx[i + 2]);
            }
        }
    }


}

void play_random(unsigned char count) {
    while (count--) {
        unsigned int x = random();
        PORTA = (unsigned char) (x >> 16);
        PORTB = (unsigned char) x;

        wait_hms(1);
    }
}

void play_rotate(unsigned int pattern, unsigned char turns, unsigned char reversed) {
    while (turns--) {
        PORTA = REORDERA((unsigned char) (pattern & 0x7F));
        PORTB = ((unsigned char) (pattern >> 7));
        __delay_ms(100);

        if(!reversed)
        {
            pattern = (pattern >> 1) | ((pattern & 1) << 13);
        }
        else
        {
            pattern = ((pattern << 1) | ((pattern & 0x2000) >> 13)) & 0x3FFF;
        }
    }
}

void play_smooth(unsigned char turns){
    unsigned char value = 5, port = 0;
    signed char inc = 1;
    int i = 0, j = 0;
    

    T1CON = 0b00100000;
    TMR1ON = 1;
    while(1)
    {
        i++;
        if(i > 500)
        {
            i=0;
            value += inc;
            if(value < 8)
            {
                j++;
                if(j < 300)
                {
                    inc = 0;
                }
                else
                {
                    j=0;
                    inc = 1;
                    
                    if(!(turns--))
                    {
                        break;
                    }
                }

            }
            else if(value > 250)
            {
                inc = -3;
            }
            else if(value > 130 && inc > 0)
            {
                inc = 3;
            }
            else if(value < 130 && inc < 0)
            {
                inc = -1;
            }   
             
        }
        
        port = TMR1L < value ? 0xFF : 0; 
        PORTB = port; 
        PORTA = port;
    }

}

int main() {
    TRISA = 0;
    TRISB = 0;



    play(STEPS_INIT, sizeof (STEPS_INIT), 3, 0);
    while (1) {

        play(STEPS_DROP, sizeof (STEPS_DROP), 1, 0);
        play(STEPS_DROP, sizeof (STEPS_DROP), 1, 1);
        play(STEPS_VSHIFT, sizeof (STEPS_VSHIFT), 10, 0);
        play(STEPS_VSHIFT, sizeof (STEPS_VSHIFT), 10, 1);
        play_smooth(14);

    }
    return 0;
}



