/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - v3.00
        Device            :  PIC16LF1823
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.20
 */

/*
Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 */

#include "mcc_generated_files/mcc.h"


const unsigned char lcd_table[][5] ={
    {0x3E, 0x51, 0x49, 0x45, 0x3E}, // 0
    {0x00, 0x42, 0x7F, 0x40, 0x00}, // 1
    {0x42, 0x61, 0x51, 0x49, 0x46}, // 2
    {0x21, 0x41, 0x45, 0x4B, 0x31}, // 3
    {0x18, 0x14, 0x12, 0x7F, 0x10}, // 4
    {0x27, 0x45, 0x45, 0x45, 0x39}, // 5
    {0x3C, 0x4A, 0x49, 0x49, 0x30}, // 6
    {0x01, 0x71, 0x09, 0x05, 0x03}, // 7
    {0x36, 0x49, 0x49, 0x49, 0x36}, // 8
    {0x06, 0x49, 0x49, 0x29, 0x1E}, // 9

    0x7e, 0x11, 0x11, 0x11, 0x7e, //A        0x80    
    0x7f, 0x49, 0x49, 0x49, 0x33, //Б        0x81    
    0x7f, 0x49, 0x49, 0x49, 0x36, //В        0x82    
    0x7f, 0x01, 0x01, 0x01, 0x03, //Г        0x83    
    0xe0, 0x51, 0x4f, 0x41, 0xff, //Д        0x84    
    0x7f, 0x49, 0x49, 0x49, 0x41, //E        0x85    
    0x77, 0x08, 0x7f, 0x08, 0x77, //Ж        0x86    
    0x41, 0x49, 0x49, 0x49, 0x36, //З        0x87    
    0x7f, 0x10, 0x08, 0x04, 0x7f, //И        0x88    
    0x7c, 0x21, 0x12, 0x09, 0x7c, //Й        0x89    
    0x7f, 0x08, 0x14, 0x22, 0x41, //K        0x8A    
    0x20, 0x41, 0x3f, 0x01, 0x7f, //Л        0x8B    
    0x7f, 0x02, 0x0c, 0x02, 0x7f, //M        0x8C    
    0x7f, 0x08, 0x08, 0x08, 0x7f, //H        0x8D    
    0x3e, 0x41, 0x41, 0x41, 0x3e, //O        0x8E    
    0x7f, 0x01, 0x01, 0x01, 0x7f, //П        0x8F    
    0x7f, 0x09, 0x09, 0x09, 0x06, //P        0x90    
    0x3e, 0x41, 0x41, 0x41, 0x22, //C        0x91
    0x01, 0x01, 0x7f, 0x01, 0x01, //T        0x92
    0x47, 0x28, 0x10, 0x08, 0x07, //У        0x93
    0x1c, 0x22, 0x7f, 0x22, 0x1c, //Ф        0x94
    0x63, 0x14, 0x08, 0x14, 0x63, //X        0x95
    0x7f, 0x40, 0x40, 0x40, 0xff, //Ц        0x96
    0x07, 0x08, 0x08, 0x08, 0x7f, //Ч        0x97
    0x7f, 0x40, 0x7f, 0x40, 0x7f, //Ш        0x98
    0x7f, 0x40, 0x7f, 0x40, 0xff, //Щ        0x99
    0x01, 0x7f, 0x48, 0x48, 0x30, //Ъ        0x9A
    0x7f, 0x48, 0x30, 0x00, 0x7f, //Ы        0x9B
    0x00, 0x7f, 0x48, 0x48, 0x30, //Э        0x9C
    0x22, 0x41, 0x49, 0x49, 0x3e, //Ь        0x9D
    0x7f, 0x08, 0x3e, 0x41, 0x3e, //Ю        0x9E
    0x46, 0x29, 0x19, 0x09, 0x7f, //Я        0x9F
    // маленькие буквы
    0x20, 0x54, 0x54, 0x54, 0x78, //a        0xA0
    0x3c, 0x4a, 0x4a, 0x49, 0x31, //б        0xA1
    0x7c, 0x54, 0x54, 0x28, 0x00, //в        0xA2
    0x7c, 0x04, 0x04, 0x04, 0x0c, //г        0xA3
    0xe0, 0x54, 0x4c, 0x44, 0xfc, //д        0xA4
    0x38, 0x54, 0x54, 0x54, 0x18, //e        0xA5
    0x6c, 0x10, 0x7c, 0x10, 0x6c, //ж        0xA6
    0x44, 0x44, 0x54, 0x54, 0x28, //з        0xA7
    0x7c, 0x20, 0x10, 0x08, 0x7c, //и        0xA8
    0x7c, 0x41, 0x22, 0x11, 0x7c, //й        0xA9
    0x7c, 0x10, 0x28, 0x44, 0x00, //к        0xAA
    0x20, 0x44, 0x3c, 0x04, 0x7c, //л        0xAB
    0x7c, 0x08, 0x10, 0x08, 0x7c, //м        0xAC
    0x7c, 0x10, 0x10, 0x10, 0x7c, //н        0xAD
    0x38, 0x44, 0x44, 0x44, 0x38, //o        0xAE
    0x7c, 0x04, 0x04, 0x04, 0x7c, //п        0xAF
    0x7C, 0x14, 0x14, 0x14, 0x08, //p        0xB0
    0x38, 0x44, 0x44, 0x44, 0x20, //c        0xB1
    0x04, 0x04, 0x7c, 0x04, 0x04, //т        0xB2
    0x0C, 0x50, 0x50, 0x50, 0x3C, //у        0xB3
    0x30, 0x48, 0xfc, 0x48, 0x30, //ф        0xB4
    0x44, 0x28, 0x10, 0x28, 0x44, //x        0xB5
    0x7c, 0x40, 0x40, 0x40, 0xfc, //ц        0xB6
    0x0c, 0x10, 0x10, 0x10, 0x7c, //ч        0xB7
    0x7c, 0x40, 0x7c, 0x40, 0x7c, //ш        0xB8
    0x7c, 0x40, 0x7c, 0x40, 0xfc, //щ        0xB9
    0x04, 0x7c, 0x50, 0x50, 0x20, //ъ        0xBA
    0x7c, 0x50, 0x50, 0x20, 0x7c, //ы        0xBB
    0x7c, 0x50, 0x50, 0x20, 0x00, //ь        0xBC
    0x28, 0x44, 0x54, 0x54, 0x38, //э        0xBD
    0x7c, 0x10, 0x38, 0x44, 0x38, //ю        0xBE
    0x08, 0x54, 0x34, 0x14, 0x7c  //я        0xBF
};

// Подключение дисплея Nokia 3310
#define N_DC    RC3 // D/C
#define N_RES   RC4 // RES
#define N_CE    RC5 // CE

#define N_cmd   0   // "команда"
#define N_data  1   // "данные"


//LCD Commands (basic)
#define LCD_SETFUNCTION        0x20  // basic
#define LCD_DISPLAYBLANK       0x08
#define LCD_DISPLAYNORMAL      0x0C
#define LCD_DISPLAYFLUSH       0x09
#define LCD_DISPLAYINVERSE     0x0D
#define LCD_SETYADDR           0x40
#define LCD_SETXADDR           0x80

//LCD Commands (extended)
#define LCD_SETFUNCTIONEXT     0x21  // extended
#define LCD_TEMPCOEF           0x04
#define LCD_SETBIAS            0x10
#define LCD_SETVOP             0x80

// Temperature Coefficient
#define LCD_TEMPCOEF_0         0x00
#define LCD_TEMPCOEF_1         0x01
#define LCD_TEMPCOEF_2         0x02
#define LCD_TEMPCOEF_3         0x03

// Bias System
#define LCD_BIAS_1_100         0x00	// 1:100
#define LCD_BIAS_1_80          0x01	// 1:80
#define LCD_BIAS_1_65          0x02	// 1:65
#define LCD_BIAS_1_48          0x03	// 1:48
#define LCD_BIAS_1_40          0x04	// 1:40/1:34
#define LCD_BIAS_1_24          0x05	// 1:24
#define LCD_BIAS_1_18          0x06	// 1:18/1:16
#define LCD_BIAS_1_10          0x07	// 1:10/1:9/1:8


// не оригинальный дисплей
#define NoOrig 0

// No Original Display SHIFT
// смещение для неоригинального дисплея
#define LCD_SHIFT              0x40

// Запись в ЖКИ
// data - данные
// DCbit=0 - "команда", DCbit=1 - "данные"

void lcd_write(unsigned char data, unsigned char DCbit) {
    N_DC = DCbit; // "команда/данные" 
    N_CE = 0;
    SPI_Exchange8bit(data);
    N_CE = 1;
}

// Инициализация ЖКИ
void lcd_init(void) {
    // первоначальное состояние
    // все выводы на выход
    N_CE = 1;
    N_DC = 0;
    N_RES = 0;

    // сброс LCD
    __delay_us(50); // 50us
    N_RES = 1;

    // настройка LCD
    lcd_write(LCD_SETFUNCTIONEXT, N_cmd); // расширеный набор команд
    lcd_write(LCD_SETBIAS + LCD_BIAS_1_24, N_cmd); // режим мультиплексирования - 1:48 (0x10 | 0x03)
    lcd_write(LCD_SETVOP + 45, N_cmd); // Vop контраст - 72 из 127         (0x80 |   72)
    lcd_write(LCD_TEMPCOEF + LCD_TEMPCOEF_2, N_cmd); // температурный коэфициент 2       (0x04 | 0x02)  

    // для неоригинального дисплея - смещение
    if (NoOrig == 1) {
        lcd_write(LCD_SHIFT + 5, N_cmd);
    }

    lcd_write(LCD_SETFUNCTION, N_cmd); // базовый набор команд
    lcd_write(LCD_DISPLAYNORMAL, N_cmd); // нормальный режим дисплея
    lcd_write(LCD_SETYADDR, N_cmd); // Y=0
    lcd_write(LCD_SETXADDR, N_cmd); // X=0
}

// Установить курсор на X,Y

void lcd_gotoxy(unsigned char x, unsigned char y) {
    // проверка предельного значения
    if (x > 83) {
        x = 78;
    }
    if (y > 5) {
        y = 5;
    }

    // для неоригинального дисплея - смещение
    if (NoOrig == 1) {
        ++y;
    }

    lcd_write(LCD_SETXADDR | x, N_cmd);
    lcd_write(LCD_SETYADDR | y, N_cmd);
}


// Очистить экран

void lcd_cls(void) {
    lcd_gotoxy(0, 0);

    for (unsigned int i = 0; i < 714; i++) {
        lcd_write(0, N_data);
    }
    lcd_gotoxy(0, 0);
}

// Вывод символа
// x - координата по оси Х [0..83]
// y - координата по оси Y [0..5]
// *pbuff - указатель на буфер с данными

void lcd_print(unsigned char x, unsigned char y, unsigned char *pbuff) {
    unsigned char i;

    lcd_gotoxy(x, y);

    for (i = 0; i < 5; i++) {
        lcd_write(pbuff[i], N_data);
    }
    lcd_write(0, N_data);
}

/*
                         Main application
 */
void main(void) {
    // initialize the device
    SYSTEM_Initialize();

    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    //INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    lcd_init(); // Инициализация ЖКИ
    lcd_cls(); // Очистить экран

    // 1 строка
    lcd_print(0, 0, lcd_table[0]);
    lcd_print(6, 0, lcd_table[1]);
    lcd_print(12, 0, lcd_table[2]);
    lcd_print(18, 0, lcd_table[3]);
    lcd_print(24, 0, lcd_table[4]);
    lcd_print(30, 0, lcd_table[5]);

    // 2 строка
    lcd_print(0, 1, lcd_table[0]);
    lcd_print(6, 1, lcd_table[1]);
    lcd_print(12, 1, lcd_table[2]);
    lcd_print(18, 1, lcd_table[3]);
    lcd_print(24, 1, lcd_table[4]);
    lcd_print(30, 1, lcd_table[5]);
    lcd_print(36, 1, lcd_table[6]);
    lcd_print(42, 1, lcd_table[7]);
    lcd_print(48, 1, lcd_table[8]);
    lcd_print(54, 1, lcd_table[9]);
    lcd_print(60, 1, lcd_table[0]);
    lcd_print(66, 1, lcd_table[1]);
    lcd_print(72, 1, lcd_table[2]);
    lcd_print(78, 1, lcd_table[7]);

    lcd_print(0, 2, lcd_table[10]);
    lcd_print(6, 2, lcd_table[11]);
    lcd_print(12, 2, lcd_table[12]);
    lcd_print(18, 2, lcd_table[13]);
    lcd_print(24, 2, lcd_table[14]);
    lcd_print(30, 2, lcd_table[15]);
    lcd_print(36, 2, lcd_table[16]);
    lcd_print(42, 2, lcd_table[17]);
    lcd_print(48, 2, lcd_table[18]);
    lcd_print(54, 2, lcd_table[19]);
    lcd_print(60, 2, lcd_table[20]);
    lcd_print(66, 2, lcd_table[21]);
    lcd_print(72, 2, lcd_table[22]);
    lcd_print(78, 2, lcd_table[23]);

    lcd_print(0, 3, lcd_table[24]);
    lcd_print(6, 3, lcd_table[25]);
    lcd_print(12, 3, lcd_table[26]);
    lcd_print(18, 3, lcd_table[27]);
    lcd_print(24, 3, lcd_table[28]);
    lcd_print(30, 3, lcd_table[29]);
    lcd_print(36, 3, lcd_table[30]);
    lcd_print(42, 3, lcd_table[31]);
    lcd_print(48, 3, lcd_table[32]);
    lcd_print(54, 3, lcd_table[33]);
    lcd_print(60, 3, lcd_table[34]);
    lcd_print(66, 3, lcd_table[35]);
    lcd_print(72, 3, lcd_table[36]);
    lcd_print(78, 3, lcd_table[37]);
    
    
    lcd_gotoxy(0, 4);
    for (char i = 0, k = 1, j = 1; i < 85; i++) {
        k = j ? k << 1 : k >> 1;
        if(k & 0x80)
        {
            j = 0;
        }
        if(k & 1)
        {
            j = 1;
        }
        lcd_write(k, N_data);
    }  

    while (1) {

        // Add your application code
    }
}
/**
 End of File
 */