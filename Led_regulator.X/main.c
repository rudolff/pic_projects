
#define RS RA4
#define EN RA5
#define D4 RC0
#define D5 RC1
#define D6 RC2
#define D7 RC4
#define MINUS_BTN RA1
#define PLUS_BTN RA0
#define MODE_BTN RA3

#define MINUS_BTN_CODE 1
#define PLUS_BTN_CODE 2
#define MODE_BTN_CODE 4

#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include "lcd.h";
#include "lcd_rus.h";



#define RED 0
#define GREEN 1
#define BLUE 2


const char RED_STR[] = {'K', 'p', 'a', 'c', n_, yi_, y_, 0};
const char GREEN_STR[] = {'3', 'e', l_, yo_, n_, yi_, y_, 0};
const char BLUE_STR[] = {'C', i_, n_, i_, y_, 0};
const char COLOR_STR[] = {TS_, v_, 'e', t_, 0};


const char STATIC_STR[] = {'C', t_, 'a', t_, i_, tch_, n_, '.', 0};
const char RAINBOW_STR[] = {'P', 'a', d_, 'y', g_, 'a', 0};

const char RANDOM_STR[] = {'C', l_, 'y', tch_, 'a', yi_, n_, '.', 0};
const char YES_STR[] = {D_, 'a', 0};
const char NO_STR[] = {'H', 'e', t_, 0};

const char BRIGHT_STR[] = {YA_, 'p', k_, 'o', 'c', t_, mz_, 0};
const char SPEED_STR[] = {'C', k_, 'o', 'p', 'o', 'c', 't', mz_, 0};

enum 
{
    NONE,
    CONST_COLOR,
    CONST_COLOR_RGB,
    SET_BRIGHT,
    SMOOTH_CHANGE,
    RANDOM_CHANGE
};

void ShowProgress(char value) {
    char str[9];
    char i = 0;
    str[8] = 0;
    if(value >= 100)
    {
        str[i++] = value / 100 + 0x30;
        value = value % 100;
    }
    
    str[i++] = value / 10 + 0x30;
    str[i++] = (value % 10) + 0x30;   
    
    for (; i < 8; ++i) {
        str[i] = (i - 2) < value / 16 ? 0xFF : 0x2D;
    }



    Lcd_Set_Cursor(2, 1);
    Lcd_Write_String(str);
}


char RGB_values[3];

void set_color(char r, char g, char b) {

    PWM1_LoadDutyValue(r>>1);
    PWM2_LoadDutyValue(g>>1);
    PWM3_LoadDutyValue(b>>1);
}

void set_component(char value, char comp) {
    switch (comp) {
        case 0:
            set_color(value, RGB_values[1], RGB_values[2]);
            break;
        case 1:
            set_color(RGB_values[0], value, RGB_values[2]);
            break;
        case 2:
            set_color(RGB_values[0], RGB_values[1], value);
            break;
    }
    ShowProgress(value);
}

inline char read_keys_port()
{
    char ret = 0;
    if(!MINUS_BTN) {
        ret |= MINUS_BTN_CODE;
    }
    if(!PLUS_BTN) {
        ret |= PLUS_BTN_CODE;
    }
    if(!MODE_BTN) {
        ret |= MODE_BTN_CODE;
    } 
    return ret;
}

char read_keys()
{
    char ret = read_keys_port();
    
    if(ret) { 
        for(char i = 0, btns; i<3; i++)
        {
            __delay_ms(50);
            btns = read_keys_port();
            if(btns != ret)
            {
                break;
            }
        }
    }
    return ret;
}

void change_mode(char mode)
{
    Lcd_Clear();
    Lcd_Set_Cursor(1, 1);
    switch(mode)
    {
        case CONST_COLOR:
            Lcd_Write_String(COLOR_STR);           
            break;

        case SMOOTH_CHANGE:
            Lcd_Write_String(RAINBOW_STR);           
            break;    
        
        case SET_BRIGHT:
            Lcd_Write_String(BRIGHT_STR);
            break;
    }

}

const char SIN_TABLE[] = {
    50, 54, 58, 63, 67, 71, 75, 78, 82, 85, 88, 90, 93, 95, 97, 98, 99, 99, 100,
    99, 99, 98, 97, 95, 93, 90, 88, 85, 82, 78, 75, 71, 67, 63, 58, 54,
    50, 46, 42, 37, 33, 29, 25, 22, 18, 15, 12, 10, 7, 5, 3, 2, 1, 1, 0,
    1, 1, 2, 3, 5, 7, 10, 12, 15, 18, 22, 25, 29, 33, 37, 42, 46
};

int main() {
    OSCILLATOR_Initialize();
    SYSTEM_Initialize();
    Lcd_Init();
    char mode = CONST_COLOR, prev_mode = NONE;
    char str[9];
    str[8] = 0;
   
    char level = 50, inc = 10;
    char n = sizeof(SIN_TABLE), i = 0, j = n/3, k = (n/3)*2;
    
    Lcd_Clear();
    Lcd_Set_Cursor(1, 1);
    //Lcd_Write_String(RAINBOW_STR);
    Lcd_Set_Cursor(2, 1);
    Lcd_Write_String("-");
    Lcd_Set_Cursor(2, 4);
    Lcd_Write_String("+");

    
    while (1) {
        char keys = read_keys();
         
        switch(mode)
        { 
            case CONST_COLOR:
                if(keys & MINUS_BTN_CODE) {
                    i--, j--, k--;
                    if(i == 255) {
                        i = n - 1;
                    }
                    if(j == 255) {
                        j = n - 1;
                    }
                    if(k == 255) {
                        k = n - 1;
                    }
                    ShowProgress(i);
                }
                else if(keys & PLUS_BTN_CODE) {
                    i++, j++, k++;
                    if(i == n) {
                        i = 0;
                    }
                    if(j == n) {
                        j = 0;
                    }
                    if(k == n) {
                        k = 0;
                    }
                    ShowProgress(i);
                }
                else if(keys & MODE_BTN_CODE) {
                    prev_mode = mode;
                    mode = SET_BRIGHT;
                    change_mode(mode);
                }

                set_color((SIN_TABLE[i] * level)/100, (SIN_TABLE[j] * level)/100 ,(SIN_TABLE[k] * level)/100 );

                break;
            
            case SMOOTH_CHANGE:

                for(char n = sizeof(SIN_TABLE), i = 0, j = n/3, k = (n/3)*2; i < n; i++, j++, k++)
                {
                    if(i == n) {
                        i = 0;
                    }
                    if(j == n) {
                        j = 0;
                    }
                    if(k == n) {
                        k = 0;
                    }                    
                    set_color((SIN_TABLE[i] * level)/100, (SIN_TABLE[j] * level)/100 ,(SIN_TABLE[k] * level)/100 );
                }
                break;

            case SET_BRIGHT:
                if(keys & MINUS_BTN_CODE) {
                    if(level != 0) {
                        level--;
                    }
                    Lcd_Clear();
                    Lcd_Set_Cursor(1, 1);
                    Lcd_Write_String(BRIGHT_STR);
                    ShowProgress(level);
                }
                else if(keys & PLUS_BTN_CODE) {
                    if(level < 100) {
                        level++;
                    }
                    Lcd_Clear();
                    Lcd_Set_Cursor(1, 1);
                    Lcd_Write_String(BRIGHT_STR);
                    ShowProgress(level);
                }
                else if(keys & MODE_BTN_CODE) {
                    mode = prev_mode;
                    prev_mode = NONE;
                    change_mode(mode);
                }
                break;

        }
        
        
        __delay_ms(50); 
        

    }
    return 0;
}