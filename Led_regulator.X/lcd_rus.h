/* 
 * File:   lcd_rus.h
 * Author: Виталий
 *
 * Created on 28 февраля 2016 г., 13:39
 */

#ifndef LCD_RUS_H
#define	LCD_RUS_H

#define B_ 160
#define G_ 161
#define YO_ 162
#define ZH_ 163
#define Z_ 164
#define I_ 165
#define Y_ 166
#define L_ 167
#define P_ 168
#define U_ 169
#define F_ 170
#define TCH_ 171
#define SH_ 172
#define TVZ_ 173
#define YI_ 174
#define EE_ 175
#define YU_ 176
#define YA_ 177

#define b_ 178
#define v_ 179
#define g_ 180
#define yo_ 181 //ё
#define zh_ 182 //ж
#define z_ 183
#define i_ 184
#define y_ 185
#define k_ 186
#define l_ 187
#define m_ 188
#define n_ 189
#define p_ 190
#define t_ 191
#define tch_ 192 //ч
#define sh_ 193 //ш
#define tvz_ 194 //ъ
#define yi_ 195 //ы
#define mz_ 196 //ь
#define ee_ 197 //э
#define yu_ 198 //ю
#define ya_ 199 //я

#define D_ 224 //Д
#define TS_ 225 //Ц
#define TSH_ 226 //Щ
#define d_ 227 //д
#define f_ 228 //ц
#define ts_ 229 //ф
#define tsh_ 230 //щ


#endif	/* LCD_RUS_H */

