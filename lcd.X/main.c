#define _XTAL_FREQ 4000000

#define RS RB4
#define EN RB5
#define D4 RB0
#define D5 RB1
#define D6 RB2
#define D7 RB3


#include <xc.h>
#include "lcd.h";

// BEGIN CONFIG
#pragma config "FOSC = INTOSCIO" // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF // Watchdog Timer Enable bit (WDT enabled)
#pragma config "MCLRE = OFF" 
#pragma config PWRTE = ON // Power-up Timer Enable bit (PWRT disabled)
#pragma config BOREN = OFF // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config CP = OFF // Flash Program Memory Code Protection bit (Code protection off)
//END CONFIG

#define B_ 160
#define G_ 161
#define YO_ 162
#define ZH_ 163
#define Z_ 164
#define I_ 165
#define Y_ 166
#define L_ 167
#define P_ 168
#define U_ 169
#define F_ 170
#define TCH_ 171
#define SH_ 172
#define TVZ_ 173
#define YI_ 174
#define EE_ 175
#define YU_ 176
#define YA_ 177

#define b_ 178
#define v_ 179
#define g_ 180
#define yo_ 181 //ё
#define zh_ 182 //ж
#define z_ 183
#define i_ 184
#define y_ 185
#define k_ 186
#define l_ 187
#define m_ 188
#define n_ 189
#define p_ 190
#define t_ 191
#define tch_ 192 //ч
#define sh_ 193 //ш
#define tvz_ 194 //ъ
#define yi_ 195 //ы
#define mz_ 196 //ь
#define ee_ 197 //э
#define yu_ 198 //ю
#define ya_ 199 //я

#define D_ 224 //Д
#define TS_ 225 //Ц
#define TSH_ 226 //Щ
#define d_ 227 //д
#define f_ 228 //ц
#define ts_ 229 //ф
#define tsh_ 230 //щ


#define RED 0
#define GREEN 1
#define BLUE 2


const char RED_STR[] = {'K', 'p', 'a', 'c', n_, yi_, y_, 0};
const char GREEN_STR[] = {'3', 'e', l_, yo_, n_, yi_, y_, 0};
const char BLUE_STR[] = {'C', i_, n_, i_, y_, 0};



void ShowProgress(char value)
{
    char str[9];
    str[8] = 0;
    for(char i=2; i<8; ++i)
    {
        str[i] = (i-2) < value/16 ? 0xFF : 0x2D;
    }
    
    str[0] = value / 10 + 0x30;
    str[1] = (value % 10) + 0x30;
    
    Lcd_Set_Cursor(2,1);
    Lcd_Write_String(str);
}

const char MAX = 100;

const char RMASK = ~1;
const char GMASK = ~2;
const char BMASK = ~4;

char Order[] = {~1, ~2, ~4};
char Deltas[] = {~0, ~0, ~0, ~100};
char RGB_values[3];


void set_color(char r, char g, char b)
{
    GIE = 0;
    
    RGB_values[0] = r;
    RGB_values[1] = g;
    RGB_values[2] = b;
    const char Masks[] = {~1, ~2, ~4};
    
    char max_i = 0, min_i = 0, mid_i=0;
    
    for(char i = 0; i<3; i++)
    {
        if(RGB_values[i] > RGB_values[max_i])
        {
            max_i = i;
        }
        else if(RGB_values[i] < RGB_values[min_i])
        {
            min_i = i;
        }
    }

    for(char i = 0; i<3; i++)
    {
        if(i != max_i && i != min_i)
        {
            mid_i = i;
            break;
        }
    }    
    
    Deltas[0] = ~(RGB_values[min_i]);
    Deltas[1] = ~(RGB_values[mid_i] - RGB_values[min_i]);
    Deltas[2] = ~(RGB_values[max_i] - RGB_values[mid_i]);
    Deltas[3] = ~(100 - RGB_values[max_i]);
    Order[0] = Masks[min_i];
    Order[1] = Masks[mid_i];
    Order[2] = Masks[max_i];
    GIE = 1;
}

void interrupt on_timer()
{
    static char i = 0;
    static char port_mirror = 0;
    
    if(T0IF)
    {
        T0IF = 0;     
        TMR0 = Deltas[i]-2;
 
        if(i == 0)
        {
            port_mirror |= 7 ;
        }
        else
        {
            port_mirror &= Order[i-1];
        }

        PORTA = port_mirror;
        if(++i > 3)
        {
            i = 0;
        }


    }

}

void set_component(char value, char comp)
{
    switch(comp)
    {
        case 0:
          set_color(value, RGB_values[1], RGB_values[2]);
          break;
        case 1:
          set_color(RGB_values[0], value, RGB_values[2]);
          break;        
        case 2:
          set_color(RGB_values[0], RGB_values[1], value);
          break;
    }
    ShowProgress(value);
}

const char SIN_TABLE[] = {
    50, 54, 58, 63, 67, 71, 75, 78, 82, 85, 88, 90, 93, 95, 97, 98, 99, 99, 100,
    99, 99, 98, 97, 95, 93, 90, 88, 85, 82, 78, 75, 71, 67, 63, 58, 54, 
    50, 46, 42, 37, 33, 29, 25, 22, 18, 15, 12, 10, 7, 5, 3, 2, 1, 1, 0,
    1, 1, 2, 3, 5, 7, 10, 12, 15, 18, 22, 25, 29, 33, 37, 42, 46
    };

int main()
{
  TRISB = 0x00;
  PORTA = 0;
  TRISA = 0x00;
  Lcd_Init();
  char str[9];
  str[8]=0; 
  
  // init tmr0
  OPTION_REG &= 0b11000001;
  TMR0 = 0;  
  T0IE = 1;
  GIE = 1;
  
  while(1)
  {
      
//    Lcd_Clear();
//    Lcd_Set_Cursor(1,1);
//    Lcd_Write_String(RED_STR);
    
    for(char n = sizeof(SIN_TABLE), i = 0, j = n/3, k = (n/3)*2; i < n; i++, j++, k++)
    {
        j %= n; k %= n;
        set_color(SIN_TABLE[i], SIN_TABLE[j], SIN_TABLE[k]);
        __delay_ms(200); 
    }
    
//    set_color(0, 5, 25);
//    ShowProgress(1);
//    __delay_ms(5000);    
//    
//    set_color(3, 10, 35);
//    ShowProgress(3);
//    __delay_ms(5000);    
//
//    set_color(25, 25, 55);
//    ShowProgress(25);
//    __delay_ms(5000); 
//
//    set_color(50, 50, 75);
//    ShowProgress(50);
//    __delay_ms(5000); 
//
//    set_color(75, 75, 75);
//    ShowProgress(75);
//    __delay_ms(5000); 
//
//    set_color(99, 85, 75);
//    ShowProgress(99);
//    __delay_ms(5000); 

 
  }
  return 0;
}